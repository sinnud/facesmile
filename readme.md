The following content is from initialization of Git.

=================================================================

Copy and connect the repository locally so that you can push updates 
you make and pull changes others make. Enter git clone and
the repository URL at your command line:

git clone https://sinnud@bitbucket.org/sinnud/facesmile.git

=================================================================

Below are readme related to installation:

You can create folders here like jpg, out, txt, log, modeldata

modeldata: model for recognizing face using 68 points. downloaded from web

jpg: the picture files you will use

out: the generated picture files

txt: the characterization text files for your picture

log: Python log files

Software required:

Python 3.6 with modules:

opencv_python (4.0)

numpy (1.16)

dlib (19.7)

The model file (download below, save to modeldata folder)

https://github.com/italojs/facial-landmarks-recognition-/blob/master/shape_predictor_68_face_landmarks.dat

Put your picture under jpg folder

Modify python/facesmail.py to load your jpg file, submit it

See output from out folder